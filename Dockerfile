#FROM registry.nextgis.com/toolbox-workers/base:0.0.3-ubuntu1804
FROM registry.nextgis.com/toolbox-workers/base:0.0.3-ubuntu1804-gdal


COPY . /opt/avral_test
RUN pip3 install --no-cache-dir /opt/avral_test
