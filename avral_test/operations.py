#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os

from avral import avral
from avral.operation import AvralOperation, OperationException
from avral.io.types import *
from avral.io.responce import AvralResponce


class Test(AvralOperation):
    def __init__(self):
        super(Test, self).__init__(
            name="test",
            inputs=[
                ("text", StringType(length=25)),
                ("src", FileType()),
            ],
            outputs=[
                ("result", FileType()),
            ],
        )

    def _do_work(self):
        """

        docker run --rm -t -i -v ${pwd}:/avral_test registry.nextgis.com/toolbox-worker-common-base:prod /bin/bash

        pip install --no-cache-dir /avral_test/

        """

        export_file = "result.txt"
        cmd = "python " + os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "test.py"
        )
        cmd = cmd + " --text {text} --src {src} --dst {dst}"
        cmd = cmd.format(
            text=self.getInput("text"),
            src=self.getInput("src"),
            dst=export_file
        )
        print('-'*30)
        print(cmd)
        print(os.path.exists(export_file))
        os.system(cmd)

        self.setOutput("result", export_file)
        print(self.getInput(u"text"), type(self.getInput(u"text")))
        print(self.getInput(u"src"), type(self.getInput(u"src")))
        print(dir(self))
        return ()
