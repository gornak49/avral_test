#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import argparse
from pprint import pprint

def get_args():

    p = argparse.ArgumentParser(description="Test avral file input/output")

    p.add_argument('--text',required=False, default = None, help='replace text')

    p.add_argument('--src', required=False, default ='../input.txt', help='input file')
    p.add_argument('--dst', required=False, default ='../result.txt', help='result file')

    return p.parse_args()


def process():

    args = get_args()
    pprint(args)
    for arg in vars(args):
        print(arg,type(args.src))
    print('-'*10)
    
    dst = args.dst #'../' + args.dst
    src = args.src #'../' + args.src
    with open(src,encoding='utf-8') as f:
        data = f.read()
    print(data)
    print('-'*10)
    print('data',type(data))
    print('-'*10)
    
    with open(dst,'w',encoding='utf-8') as f:
        if args.text:
            newdata = '\n'.join((
                data.upper(),
                args.text
                ))
        else:
            newdata = data
        f.write(newdata)

    with open(dst,encoding='utf-8') as f:
        data = f.read()
    print(data)
    print('-'*10)
    
    return

print('-'*30)
print('START')
print('СТАРТ')
print('-'*10)
process()
print('END')
print('-'*30)
