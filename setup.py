import os

from setuptools import setup

requires = [
    'avral',
    'argparse'
]

setup(
    name='avral_test',
    version='0.0.1',
    description='Test avral file input/output',
    classifiers=[
        "Programming Language :: Python",
    ],
    author='nextgis',
    author_email='info@nextgis.com',
    url='http://nextgis.com',
    keywords='test',
    packages=['avral_test'],
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'avral_operations': [
            'test = avral_test.operations:Test',
        ],
    }
)
